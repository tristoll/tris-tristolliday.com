import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }      from './home/home.component';
import { ContactComponent }      from './contact/contact.component';
import { PortfolioComponent }      from './portfolio/portfolio.component';
import { ProjectComponent }      from './project/project.component';

const routes: Routes = [
  { 
    path: '', 
    pathMatch: 'full', 
    data: { state: 'home'},
    children:[
      {
        path:'',
        component: HomeComponent,
      },
      {
      path:'',
      component: HomeComponent,
      outlet:'aux'
    }]
  },
  { 
    path: 'contact', 
    data: { state: 'contact'},
    children:[
      {
        path:'',
        component: ContactComponent,
      }]
  },
  { 
    path: 'portfolio', 
    data: { state: 'portfolio'},
    children:[
      {
        path:'',
        component: PortfolioComponent,
      },
      {
        path:':id',
        component: ProjectComponent,
      }]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}