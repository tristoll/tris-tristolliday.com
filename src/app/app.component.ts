import { Component } from '@angular/core';
import { routerTransition } from './router.animations';

@Component({
  selector: 'app-root',
  animations: [routerTransition],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  mx = 0
  my = 0
  positionMask(x,y){
    let valuesX = (parseInt(x) - 300) + 'px'
    let valuesY = (parseInt(y) - 300) + 'px'
    let valuesXY = valuesX + ' ' + valuesY
    let styles = {
      'mask-position' : [valuesXY],
      '-webkit-mask-position' : [valuesXY]
      };
    return styles
  }
  spotlight(event) {
    this.mx = event.pageX;
    this.my = event.pageY;
  }
  log(p){
    console.log(p)
  }
  getState(outlet) {
    //console.log(outlet.name) "PRIMARY"
    return outlet.activatedRouteData.state
  }
}
