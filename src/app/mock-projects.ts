

import { Project } from './project';

export const PROJECTS: Project[] = [
{
    "id": "gp-batteries-site",
    "name": "Footprint Micro-site",
    "imgsrc": "gpbatteriesv2.png",
    "imgsrcalt": "gpbatteriesv1.png",
    "heroimg":"/assets/gp-mobile.png",
    "heroimg2":"/assets/gp-desktop.png",
    "heroimg3":"/assets/gp-tablet.png",
    "client":"GP Batteries",
    "description":"A Full Stack, responsive, and multilingual capable microsite for GP Batteries, promoting the benefits of reuseable batteries",
    
},
{
    "id": "kandoo-site",
    "name": "The EU can do it too",
    "imgsrc": "kdh-1.png",
    "imgsrcalt": "kdh-2.png",
    "heroimg":"/assets/kd-2.png",
    "heroimg2":"/assets/kd-1.png",
    "heroimg3":"/assets/kd-3.png",
    "description":"Creating a website that could be viewed in multiple languages, including ltr and rtl, required a completely different approach",
    "client":"Kandoo"
},

{
    "id": "gerble-micro-site",
    "name": "Gerble Micro-site",
    "imgsrc": "gerble-site-v2.png",
    "imgsrcalt": "gerble-site-v1.png",
    "heroimg":"/assets/gb-pho.png",
    "heroimg2":"/assets/gb-des.png",
    "heroimg3":"/assets/gb-tab.png",
    "client":"Gerble",
    "description":"A responsive microsite, getting gerble ready for the big time",
    
},
{
    "id": "condor-site",
    "name": "Condor Multilingual",
    "imgsrc": "condorv2.png",
    "imgsrcalt": "condorv1.png",
    "heroimg":"/assets/cf-mobile.png",
    "heroimg2":"/assets/cf-desktop.png",
    "heroimg3":"/assets/cf-tablet.png",
    "description":"To keep up with increasing demand and competition, Condor Ferries needed a new, full responsive site to get bums on seats, and passengers travelling to the Islands",
    "client":"Condor Ferries"
},
{
    "id": "sweet-freedom-site",
    "name": "Sweet Freedom Site",
    "imgsrc": "sfsitev2.png",
    "imgsrcalt": "sfsitev1.png",
    "heroimg":"/assets/sf-mobile.png",
    "heroimg2":"/assets/sf-desktop.png",
    "heroimg3":"/assets/sf-tablet.png",
    "description":"Sweet freedom had just rebranded their packaging, and needed a modern responsive site to match.",
    "client":"Sweet Freedom"
},
{
    "id": "urban-site",
    "name": "Building a Fit New Site",
    "imgsrc": "urbh2.png",
    "imgsrcalt": "urbh1.png",
    "heroimg":"/assets/urb-m.png",
    "heroimg2":"/assets/urb-d.png",
    "heroimg3":"/assets/urb-t.png",
    "description":"A responsive website using Umbraco, Foundation and Angular to serve Urban's growing community. Current build is still in production",
    "client":"Urban Health and Fitness"
},
{
    "id": "walker-site",
    "name": "A highly customisable agency site",
    "imgsrc": "wah2.png",
    "imgsrcalt": "wah1.png",
    "heroimg":"/assets/wa1.png",
    "heroimg2":"/assets/wa2.png",
    "heroimg3":"/assets/wa3.png",
    "description":"Creating a website to showcase the whole breadth of work required something highly adaptable and modular. Using a mix of Umbraco and Angular to create modules allowed any possible layout to be envisioned",
    "client":"Walker Agency"
},
{
    "id": "eurolines-site",
    "name": "Eurolines Multilingual",
    "imgsrc": "eurh2.png",
    "imgsrcalt": "eurh1.png",
    "heroimg":"/assets/eur-m.png",
    "heroimg2":"/assets/eur-d.png",
    "heroimg3":"/assets/eur-t.png",
    "description":" Building a multilingual site for a company that travels to over 100 destinations, required a clever use of layout, to ensure things didn't get too repetitive.\n (Eurolines UK has since become National Express)",
    "client":"Eurolines UK"
}];

