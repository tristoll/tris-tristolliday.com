import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'top-navigation',
  templateUrl: './topnavigation.component.html',
  styleUrls: ['./topnavigation.component.scss']
})
export class TopnavigationComponent implements OnInit {
  links = [{
    "id":"home-link",
    "name":"Tris Tolliday",
    "link":"/"
  },
  {
    "id":"contact",
    "name":"Contact",
    "link":"/contact"
  },
  {
    "id":"portfolio",
    "name":"Portfolio",
    "link":"/portfolio"
  }]

  constructor() { }

  ngOnInit() {
  }

}
