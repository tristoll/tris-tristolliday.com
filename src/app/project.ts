export class Project {
    id: string
    name: string
    imgsrc: string
    imgsrcalt: string
    heroimg: string
    heroimg2: string
    heroimg3: string
    description: string
    client: string
}